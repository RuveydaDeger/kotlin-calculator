package com.example.ruveydadeger.kotlinhesapmakinesi

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.widget.Toast
import com.example.ruveydadeger.kotlinhesapmakinesi.dialog.OptionDialog
import com.example.ruveydadeger.kotlinhesapmakinesi.dialog.SignUpDialog
import com.example.ruveydadeger.kotlinhesapmakinesi.helper.LoginHelper
import com.example.ruveydadeger.kotlinhesapmakinesi.listener.LoginResponseListener
import kotlinx.android.synthetic.main.activity_login.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class LoginActivity : AppCompatActivity(), LoginResponseListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        buttonGiris.setOnClickListener {
            val username = editTextUserName.text.toString()
            val password = editTextPassword.text.toString()
            val loginHelper: LoginHelper = LoginHelper(this, editTextUserName.text.toString(), editTextPassword.text.toString(), this)
            if (username.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, getString(R.string.Username_or_password_can_not_be_empty), Toast.LENGTH_SHORT).show()
            } else {
                loginHelper.checkUser()
            }
        }

        buttonSignUp.setOnClickListener {
            val dialog: SignUpDialog = SignUpDialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.show()
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onLoginSuccesful() {
        val dialog: OptionDialog = OptionDialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.show()
    }

    override fun onLoginFailure(error: String) {

        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }
}











