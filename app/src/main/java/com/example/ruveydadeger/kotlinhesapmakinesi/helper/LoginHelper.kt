package com.example.ruveydadeger.kotlinhesapmakinesi.helper

import android.content.Context
import com.example.ruveydadeger.kotlinhesapmakinesi.R
import com.example.ruveydadeger.kotlinhesapmakinesi.listener.LoginResponseListener
import com.example.ruveydadeger.kotlinhesapmakinesi.util.DBUtils

class LoginHelper {
    var mUsername: String
    var mPassword: String
    var mContext: Context
    var mListener: LoginResponseListener

    constructor(context: Context, username: String, password: String, listener: LoginResponseListener) {
        mContext = context
        mUsername = username
        mPassword = password
        this.mListener = listener
    }

    /**
     * @author rüveyda.deger
     *
     * */
    fun checkUser() {
        if (!DBUtils.isThisUserValid(mUsername, mPassword)) {
            mListener.onLoginFailure(mContext.getString(R.string.Username_or_password_is_incorrect))
        } else {
            mListener.onLoginSuccesful()
        }
    }
}



