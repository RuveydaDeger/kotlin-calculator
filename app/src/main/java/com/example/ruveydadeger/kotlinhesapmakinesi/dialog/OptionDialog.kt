package com.example.ruveydadeger.kotlinhesapmakinesi.dialog

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.ruveydadeger.kotlinhesapmakinesi.ListActivity
import com.example.ruveydadeger.kotlinhesapmakinesi.MainActivity
import com.example.ruveydadeger.kotlinhesapmakinesi.R
import com.example.ruveydadeger.kotlinhesapmakinesi.RecyclerActivity
import kotlinx.android.synthetic.main.dialog_option.*

class OptionDialog(context: Context?) : Dialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_option)

        buttonCalculator.setOnClickListener {
            val hesapGecis = Intent(context, MainActivity::class.java)

            context.startActivity(hesapGecis)
        }
        buttonList.setOnClickListener {
            val listeGecis = Intent(context, ListActivity::class.java)

            context.startActivity(listeGecis)
        }

        buttonList1.setOnClickListener {
            val recyclerGecis = Intent(context, RecyclerActivity::class.java)
            context.startActivity(recyclerGecis)
        }
    }
}