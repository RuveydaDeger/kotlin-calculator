package com.example.ruveydadeger.kotlinhesapmakinesi.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import com.example.ruveydadeger.kotlinhesapmakinesi.R
import com.example.ruveydadeger.kotlinhesapmakinesi.`object`.RecyclerListItem
import com.example.ruveydadeger.kotlinhesapmakinesi.adapter.RecyclerListAdapter
import com.example.ruveydadeger.kotlinhesapmakinesi.enum.FoodTypes
import com.example.ruveydadeger.kotlinhesapmakinesi.util.DBUtils
import kotlinx.android.synthetic.main.dialog_category.*


class CategoryDialog(context: Context?, private val value: String, private var adapter: RecyclerListAdapter) : Dialog(context) {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_category)

        var selectedFoodType: Int = 0
        buttonSave.setOnClickListener {
            if (radioButtonFood.isChecked) {
                selectedFoodType = FoodTypes.FOOD.value()

            } else if (radioButtonDrink.isChecked) {
                selectedFoodType = FoodTypes.DRINK.value()

            } else if (radioButtonOther.isChecked) {
                selectedFoodType = FoodTypes.OTHER.value()

            }
            if (selectedFoodType > 0 && selectedFoodType < 4) {
                if (DBUtils.addToReceylerList(RecyclerListItem(value, selectedFoodType))) {
                    Toast.makeText(context, context.getString(R.string.successfully_saved), Toast.LENGTH_SHORT).show()
                    this.dismiss()
                    adapter!!.changeList(DBUtils.getAllRecyclerListObj())
                }
            } else {
                Toast.makeText(context, context.getString(R.string.choose_one), Toast.LENGTH_SHORT).show()
            }
        }
    }
}