package com.example.ruveydadeger.kotlinhesapmakinesi

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.Toast
import com.example.ruveydadeger.kotlinhesapmakinesi.`object`.ListItem
import com.example.ruveydadeger.kotlinhesapmakinesi.adapter.ListAdapter
import com.example.ruveydadeger.kotlinhesapmakinesi.util.DBUtils
import kotlinx.android.synthetic.main.activity_listview.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class ListActivity : AppCompatActivity(), AdapterView.OnItemClickListener, TextWatcher {

    var mSelectedItem: ListItem = ListItem()
    var listAdapter: ListAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listview)
        editText.addTextChangedListener(this)

        val recordList: List<ListItem> = DBUtils.getAllListObj()
        resetSelections(recordList)
        listAdapter = ListAdapter(this, recordList)
        listView.adapter = listAdapter

        ButtonAdd.setOnClickListener {
            var value = editText.text.toString()
            if (value.isEmpty() || value.equals(" ", true)) {
                Toast.makeText(this,getString(R.string.can_not_be_empty), Toast.LENGTH_SHORT).show()
                //boş olamaz

            } else if (DBUtils.isThisExist(value)) {
                editText.setText("")
                Toast.makeText(this, getString(R.string.already_have), Toast.LENGTH_SHORT).show()
                //zaten kayıtlı
            } else {
                // kaydetme islemi yap
                if (DBUtils.addToList(ListItem(value))) {
                    editText.setText("")
                    Toast.makeText(this, getString(R.string.successfully_saved), Toast.LENGTH_SHORT).show()
                    // Başarıyla kaydedildi
                }
            }
            listAdapter!!.changeList(DBUtils.getAllListObj())

        }
        ButtonDel.setOnClickListener {
            DBUtils.delListing(mSelectedItem)
            listAdapter!!.changeList(DBUtils.getAllListObj())
        }

        ButtonDelEveryting.setOnClickListener {
            DBUtils.delEverytingListing()
            listAdapter!!.changeList(DBUtils.getAllListObj())
        }

        listView.setOnItemClickListener(this)

        imageButtonTextDel.setOnClickListener {
            editText.setText("")
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0)

        }
    }

    fun resetSelections(tmpList: List<ListItem>) {
        for (item: ListItem in tmpList) {
            DBUtils.changeSelection(item, false)
        }
    }

    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        mSelectedItem = DBUtils.getAllListObj().get(p2)
        resetSelections(DBUtils.getAllListObj())
        DBUtils.changeSelection(mSelectedItem, true)
        listAdapter!!.changeList(DBUtils.getAllListObj())

    }

    override fun afterTextChanged(p0: Editable?) {

    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        listAdapter!!.filter(p0.toString().replace(" ", ""))
    }
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

}








