package com.example.ruveydadeger.kotlinhesapmakinesi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.ruveydadeger.kotlinhesapmakinesi.ListActivity
import com.example.ruveydadeger.kotlinhesapmakinesi.R
import com.example.ruveydadeger.kotlinhesapmakinesi.`object`.ListItem


open class ListAdapter(val activity: ListActivity, list: List<ListItem>) : BaseAdapter() {

    var mList: MutableList<ListItem>? = list as MutableList<ListItem>
    var mOrignList: List<ListItem> = list
    var mActivity: ListActivity = activity
    private val mInflater: LayoutInflater

    init {
        this.mInflater = LayoutInflater.from(mActivity)

    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val satirView: View

        satirView = mInflater.inflate(R.layout.item_listview, null)

        var item: ListItem = getItem(p0) as ListItem

        val textViewName: TextView = satirView.findViewById(R.id.textViewName) as TextView
        textViewName.text = item.value

        textViewName.setBackgroundColor(
                if (item.selected)
                    mActivity.resources.getColor(R.color.selectedItemColor)
                else
                    mActivity.resources.getColor(R.color.unSelectedItemColor)
        )

        return satirView
    }

    override fun getItem(p0: Int): Any {
        return mList!!.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return mList!!.size
    }

    fun changeList(tmpList: List<ListItem>) {
        mList = tmpList as MutableList<ListItem>
        mOrignList = tmpList
        notifyDataSetChanged()
    }

    fun filter(searchtext: String) {
        mList = ArrayList<ListItem>()
        for (item: ListItem in mOrignList) {
            if (item.value.toLowerCase().contains(searchtext.toLowerCase()).equals(true)) {
                mList!!.add(item)
            }
        }
        notifyDataSetChanged()
    }
}



