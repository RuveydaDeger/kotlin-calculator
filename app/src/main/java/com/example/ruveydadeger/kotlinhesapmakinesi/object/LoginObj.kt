package com.example.ruveydadeger.kotlinhesapmakinesi.`object`

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class LoginObj() : RealmObject() {
    @PrimaryKey open var username: String = ""
    open var password: String = ""

    constructor(username: String, password: String) : this() {
        this.username = username
        this.password = password
    }
}