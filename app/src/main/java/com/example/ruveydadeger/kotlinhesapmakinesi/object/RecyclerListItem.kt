package com.example.ruveydadeger.kotlinhesapmakinesi.`object`

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class RecyclerListItem() : RealmObject() {
    @PrimaryKey open var value: String = ""
    var selected:Boolean = false
    var foodCategory : Int ? = null
    constructor(value: String) : this() {
        this.value = value
    }
    constructor(value: String, foodCategory:Int) : this() {
        this.value = value
        this.foodCategory = foodCategory
    }
}