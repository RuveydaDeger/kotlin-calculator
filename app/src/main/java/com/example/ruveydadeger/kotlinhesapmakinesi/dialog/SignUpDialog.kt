package com.example.ruveydadeger.kotlinhesapmakinesi.dialog

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.ruveydadeger.kotlinhesapmakinesi.MainActivity
import com.example.ruveydadeger.kotlinhesapmakinesi.R
import com.example.ruveydadeger.kotlinhesapmakinesi.`object`.LoginObj
import com.example.ruveydadeger.kotlinhesapmakinesi.util.DBUtils
import kotlinx.android.synthetic.main.dialog_sign_up.*


class SignUpDialog(context: Context?) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_sign_up)
        buttonSignUp.setOnClickListener {
            var username = editTextUserName.text.toString()
            var password = editTextPassword.text.toString()
            if (username.isEmpty() || password.isEmpty()) {
                Toast.makeText(context, context.getString(R.string.Username_or_password_can_not_be_empty), Toast.LENGTH_SHORT).show()// Kullanıcı adı veya şifre boş olamaz

            } else if (DBUtils.isThisUserValid(username, password)) {
                Toast.makeText(context, context.getString(R.string.already_registered), Toast.LENGTH_SHORT).show()//zaten kayıtlısınız

            } else {
                // kaydetme islemi yap
                if (DBUtils.addLoginUser(LoginObj(username, password))) {
                    Toast.makeText(context, context.getString(R.string.successfully_saved), Toast.LENGTH_SHORT).show()// Başarıyla kaydedildi
                    this.dismiss()
                }
            }
        }
    }
}