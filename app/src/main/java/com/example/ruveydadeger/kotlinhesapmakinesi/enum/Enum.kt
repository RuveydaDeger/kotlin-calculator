package com.example.ruveydadeger.kotlinhesapmakinesi.enum

enum class FoodTypes {
    NONE, FOOD, DRINK, OTHER, ALL;

    private val BASE_ORDINAL: Int = 0
    fun value(): Int {
        return ordinal + BASE_ORDINAL
    }
}

