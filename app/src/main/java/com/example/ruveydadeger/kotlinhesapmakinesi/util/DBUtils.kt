package com.example.ruveydadeger.kotlinhesapmakinesi.util

import com.example.ruveydadeger.kotlinhesapmakinesi.`object`.ListItem
import com.example.ruveydadeger.kotlinhesapmakinesi.`object`.LoginObj
import com.example.ruveydadeger.kotlinhesapmakinesi.`object`.RecyclerListItem
import com.example.ruveydadeger.kotlinhesapmakinesi.enum.FoodTypes
import io.realm.R
import io.realm.Realm
import io.realm.RealmQuery
import io.realm.RealmResults


class DBUtils {
    companion object {
        //LoginObj start
        fun addLoginUser(login: LoginObj): Boolean {
            var realm: Realm = Realm.getDefaultInstance()
            realm.beginTransaction()
            try {
                realm.copyToRealmOrUpdate(login)
                realm.commitTransaction()
                realm.close()
                return true
            } catch(e: Exception) {
                realm.cancelTransaction()
                e.printStackTrace()
                return false
            }
        }

        fun delLoginUser(login: LoginObj): Boolean {
            var realm: Realm = Realm.getDefaultInstance()
            try {
                realm.beginTransaction()
                realm.where(LoginObj::class.java).equalTo("username", login.username).findFirst().deleteFromRealm()
                realm.commitTransaction()
                realm.close()
                return true
            } catch(e: Exception) {
                realm.cancelTransaction()
                e.printStackTrace()
                return false
            }
        }

        fun getAllLoginObj(): List<LoginObj> {
            return Realm.getDefaultInstance().where(LoginObj::class.java).findAll()
        }

        fun isThisUserValid(username: String, password: String): Boolean {
            val login: LoginObj? = Realm.getDefaultInstance().where(LoginObj::class.java).equalTo("username", username).findFirst()
            return login != null && (login.username.equals(username) && login.password.equals(password))

        }

        //LoginObj end
        //ListItem start

        fun addToList(liste: ListItem): Boolean {
            var realm: Realm = Realm.getDefaultInstance()
            try {
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(liste)
                realm.commitTransaction()
                realm.close()
                return true
            } catch(e: Exception) {
                realm.cancelTransaction()
                return false
            }
        }

        fun changeSelection(liste: ListItem, isSelected: Boolean): Boolean {
            var realm: Realm = Realm.getDefaultInstance()
            try {
                realm.beginTransaction()
                liste.selected = isSelected
                realm.copyToRealmOrUpdate(liste)
                realm.commitTransaction()
                realm.close()
                return true
            } catch(e: Exception) {
                realm.cancelTransaction()
                return false
            }
        }

        fun delListing(liste: ListItem): Boolean {
            var realm: Realm = Realm.getDefaultInstance()
            try {
                realm.beginTransaction()
                realm.where(ListItem::class.java).equalTo("value", liste.value).findAll().deleteAllFromRealm()
                realm.commitTransaction()
                realm.close()
                return true
            } catch(e: Exception) {
                realm.cancelTransaction()
                return false
            }
        }

        fun delEverytingListing(): Boolean {
            var realm: Realm = Realm.getDefaultInstance()
            try {
                realm.beginTransaction()
                realm.where(ListItem::class.java).findAll().deleteAllFromRealm()
                realm.commitTransaction()
                realm.close()
                return true
            } catch(e: Exception) {
                realm.cancelTransaction()
                return false
            }
        }

        fun getAllListObj(): List<ListItem> {
            return Realm.getDefaultInstance().where(ListItem::class.java).findAll()
        }

        fun isThisExist(value: String): Boolean {
            val liste: ListItem? = Realm.getDefaultInstance().where(ListItem::class.java).equalTo("value", value).findFirst()
            return liste != null && (liste.value.equals(value))
        }

        //List end
        //Recycler start
        fun addToReceylerList(Rliste: RecyclerListItem): Boolean {
            var realm: Realm = Realm.getDefaultInstance()
            try {
                realm.beginTransaction()
                realm.copyToRealmOrUpdate(Rliste)
                realm.commitTransaction()
                realm.close()
                return true
            } catch(e: Exception) {
                realm.cancelTransaction()
                return false
            }
        }

        fun changeSelection(Rliste: RecyclerListItem, isSelected: Boolean): Boolean {
            var realm: Realm = Realm.getDefaultInstance()
            try {
                realm.beginTransaction()
                Rliste.selected = isSelected
                realm.copyToRealmOrUpdate(Rliste)
                realm.commitTransaction()
                realm.close()
                return true
            } catch(e: Exception) {
                realm.cancelTransaction()
                return false
            }
        }

        fun delRecyclerListing(Rliste: RecyclerListItem): Boolean {
            var realm: Realm = Realm.getDefaultInstance()
            try {
                realm.beginTransaction()
                realm.where(RecyclerListItem::class.java).equalTo("value", Rliste.value).findAll().deleteAllFromRealm()
                realm.commitTransaction()
                realm.close()
                return true
            } catch(e: Exception) {
                realm.cancelTransaction()
                return false
            }
        }

        fun delEverytingRecylerListing(): Boolean {
            var realm: Realm = Realm.getDefaultInstance()
            try {
                realm.beginTransaction()
                realm.where(RecyclerListItem::class.java).findAll().deleteAllFromRealm()
                realm.commitTransaction()
                realm.close()
                return true
            } catch(e: Exception) {
                realm.cancelTransaction()
                return false
            }
        }

        fun getAllRecyclerListObj(): List<RecyclerListItem> {
            return Realm.getDefaultInstance().where(RecyclerListItem::class.java).findAll()
        }

        fun isThisRecyclerExist(value: String): Boolean {
            val Rliste: RecyclerListItem? = Realm.getDefaultInstance().where(RecyclerListItem::class.java).equalTo("value", value).findFirst()
            return Rliste != null && (Rliste.value.equals(value))
        }

        //recycler end


        fun filterListByCategory(foodCategory: Int): List<RecyclerListItem> {
            if(foodCategory == FoodTypes.ALL.value()){
                return Realm.getDefaultInstance().where(RecyclerListItem::class.java).findAll()
            }else {
                return Realm.getDefaultInstance().where(RecyclerListItem::class.java).equalTo("foodCategory", foodCategory).findAll()
            }

        }
    }
}