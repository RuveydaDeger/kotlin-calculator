package com.example.ruveydadeger.kotlinhesapmakinesi

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.RadioGroup
import android.widget.Toast
import com.example.ruveydadeger.kotlinhesapmakinesi.`object`.RecyclerListItem
import com.example.ruveydadeger.kotlinhesapmakinesi.adapter.RecyclerListAdapter
import com.example.ruveydadeger.kotlinhesapmakinesi.dialog.CategoryDialog
import com.example.ruveydadeger.kotlinhesapmakinesi.enum.FoodTypes
import com.example.ruveydadeger.kotlinhesapmakinesi.util.DBUtils
import kotlinx.android.synthetic.main.activity_recycler.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


open class RecyclerActivity : AppCompatActivity(), TextWatcher, RecyclerListAdapter.RecyclerViewListener {

    var mSelectedItem: RecyclerListItem = RecyclerListItem()
    var recyclerViewAdapter: RecyclerListAdapter? = null
    var mSelectedFoodCategory: Int = FoodTypes.ALL.value()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)

        recyclerView.layoutManager = LinearLayoutManager(this)
        val recordList: List<RecyclerListItem> = DBUtils.getAllRecyclerListObj()
        recyclerViewAdapter = RecyclerListAdapter(this, DBUtils.getAllRecyclerListObj(), this)
        resetSelections(recordList)
        recyclerView.adapter = recyclerViewAdapter

        imageButtonAdd.setOnClickListener {

            var value = editText.text.toString()
            if (value.isEmpty() || value.equals(" ", true)) {
                Toast.makeText(this, getString(R.string.can_not_be_empty), Toast.LENGTH_SHORT).show()
                //boş olamaz

            } else if (DBUtils.isThisRecyclerExist(value)) {
                Toast.makeText(this, getString(R.string.already_have), Toast.LENGTH_SHORT).show()
                //zaten kayıtlı

            } else {
                // kaydetme islemi yap.
                val dialog: CategoryDialog = CategoryDialog(this, value, recyclerViewAdapter!!)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.show()
            }
            recyclerViewAdapter!!.changeList(DBUtils.getAllRecyclerListObj())
        }
        ButtonDel.setOnClickListener {
            DBUtils.delRecyclerListing(mSelectedItem)
            recyclerViewAdapter!!.changeList(DBUtils.getAllRecyclerListObj())
        }
        ButtonDelEveryting.setOnClickListener {
            DBUtils.delEverytingRecylerListing()
            recyclerViewAdapter!!.changeList(DBUtils.getAllRecyclerListObj())
        }
        imageButtonTextDel.setOnClickListener {
            editText.setText("")
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0)
        }

        radioGroup2.setOnCheckedChangeListener { radioGroup: RadioGroup, i: Int ->
            if (radioButtonFood.isChecked) {
                mSelectedFoodCategory = FoodTypes.FOOD.value()
            }
            if (radioButtonDrink.isChecked) {
                mSelectedFoodCategory = FoodTypes.DRINK.value()
            }
            if (radioButtonOther.isChecked) {
                mSelectedFoodCategory = FoodTypes.OTHER.value()
            }
            if (radioButtonAllOf.isChecked) {
                mSelectedFoodCategory = FoodTypes.ALL.value()
            }
            resetSelections(DBUtils.getAllRecyclerListObj())
            recyclerViewAdapter!!.changeList(DBUtils.filterListByCategory(mSelectedFoodCategory))
        }

        editText.addTextChangedListener(this)
    }

    fun resetSelections(tmpList: List<RecyclerListItem>) {
        for (item: RecyclerListItem in tmpList) {
            DBUtils.changeSelection(item, false)
        }
    }

    override fun onItemClickListener(item: RecyclerListItem) {
        mSelectedItem = item
        resetSelections(DBUtils.getAllRecyclerListObj())
        DBUtils.changeSelection(mSelectedItem, true)
        recyclerViewAdapter!!.changeList(DBUtils.filterListByCategory(mSelectedFoodCategory))
    }

    override fun afterTextChanged(p0: Editable?) {

    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        recyclerViewAdapter!!.filter(p0.toString().replace(" ", ""))
    }
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }
}