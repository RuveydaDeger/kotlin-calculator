package com.example.ruveydadeger.kotlinhesapmakinesi.adapter

import android.content.ReceiverCallNotAllowedException
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.ruveydadeger.kotlinhesapmakinesi.ListActivity
import com.example.ruveydadeger.kotlinhesapmakinesi.R
import com.example.ruveydadeger.kotlinhesapmakinesi.RecyclerActivity
import com.example.ruveydadeger.kotlinhesapmakinesi.`object`.ListItem
import com.example.ruveydadeger.kotlinhesapmakinesi.`object`.RecyclerListItem
import com.example.ruveydadeger.kotlinhesapmakinesi.enum.FoodTypes
import com.example.ruveydadeger.kotlinhesapmakinesi.enum.FoodTypes.FOOD

class RecyclerListAdapter(val activity: RecyclerActivity, val list: List<RecyclerListItem>, val listener: RecyclerViewListener) : RecyclerView.Adapter<RecyclerListAdapter.ViewHolder>() {

    var mList: MutableList<RecyclerListItem>? = list as MutableList<RecyclerListItem>
    var mOrignList: List<RecyclerListItem> = list
    var mListener: RecyclerViewListener? = listener

    interface RecyclerViewListener {
        fun onItemClickListener(item: RecyclerListItem)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        var item: RecyclerListItem = mList!!.get(position)
        holder!!.textName.text = item.value

        holder!!.textName.setOnClickListener {
            mListener!!.onItemClickListener(item)

        }
        holder!!.linearLayoutListContainer.setBackgroundColor(
                if (item.selected)
                    activity.resources.getColor(R.color.selectedItemColor)
                else
                    activity.resources.getColor(R.color.unSelectedItemColor)
        )
        if (item.foodCategory == FoodTypes.FOOD.value()) {
            holder!!.imageView.setImageDrawable(activity.getDrawable(R.drawable.food))
        } else if (item.foodCategory == FoodTypes.DRINK.value()) {
            holder!!.imageView.setImageDrawable(activity.getDrawable(R.drawable.drink))
        } else if (item.foodCategory == FoodTypes.OTHER.value()) {
            holder!!.imageView.setImageDrawable(activity.getDrawable(R.drawable.other))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.item_recyclerview, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    class ViewHolder : RecyclerView.ViewHolder {
        var textName: TextView
        var imageView: ImageView
        var linearLayoutListContainer: LinearLayout


        constructor(view: View) : super(view) {
            textName = itemView.findViewById(R.id.textName) as TextView
            imageView = itemView.findViewById(R.id.imageView) as ImageView
            linearLayoutListContainer = itemView.findViewById(R.id.linearLayoutListContainer) as LinearLayout

        }
    }

    fun changeList(tmpList: List<RecyclerListItem>) {
        mList = tmpList as MutableList<RecyclerListItem>
        mOrignList = tmpList
        this.notifyDataSetChanged()

    }


    fun filter(searchtext: String) {
        mList = ArrayList<RecyclerListItem>()
        for (item: RecyclerListItem in mOrignList) {
            if (item.value.toLowerCase().contains(searchtext.toLowerCase()).equals(true)) {
                mList!!.add(item)
            }
        }
        this.notifyDataSetChanged()
    }
}


