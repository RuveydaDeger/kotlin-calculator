package com.example.ruveydadeger.kotlinhesapmakinesi

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import android.graphics.Typeface
import android.view.KeyEvent.KEYCODE_BACK
import android.view.KeyEvent.KEYCODE_BACK
import android.text.InputFilter
import android.widget.Button
import io.realm.Realm


class MainActivity : AppCompatActivity() {

    val realm by lazy { Realm.getDefaultInstance() }
    var mIlkDeger: Double? = null
    var mSonuc: Double? = null
    var mIslem: String? = null
    var mYeniIslem: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button0.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.button0)
            }
        })

        button1.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.button1)
            }
        })
        button2.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.button2)
            }
        })
        button3.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.button3)
            }
        })
        button4.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.button4)
            }
        })
        button5.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.button5)
            }
        })
        button6.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.button6)
            }
        })
        button7.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.button7)
            }
        })
        button8.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.button8)
            }
        })
        button9.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.button9)
            }
        })
        buttonNokta.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonNumbersClicked(R.id.buttonNokta)
            }
        })
        buttonEsittir.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonOperatorClicked(R.id.buttonEsittir)
            }
        })
        buttonYuzde.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonOperatorClicked(R.id.buttonYuzde)
            }
        })
        buttonTopla.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonOperatorClicked(R.id.buttonTopla)
            }
        })
        buttonCarpma.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonOperatorClicked(R.id.buttonCarpma)
            }
        })
        buttonCikarma.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonOperatorClicked(R.id.buttonCikarma)
            }
        })
        buttonBolme.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonOperatorClicked(R.id.buttonBolme)
            }
        })
        buttonSifirla.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonOperatorClicked(R.id.buttonSifirla)
            }
        })

        buttonSil.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                buttonOperatorClicked(R.id.buttonSil)
            }
        })
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    private fun updateFirstValueAndClearHesap() {
        if (!textViewHesap.getText().toString().isEmpty() && !textViewHesap.getText().toString().equals(" ")) {
            mIlkDeger = textViewHesap.getText().toString().toDouble()
            textViewHesap.setText("")
        } else {
            if (mIlkDeger == null) {
                Toast.makeText(this, getString(R.string.enter_value), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun buttonNumbersClicked(id: Int) {
        if (mYeniIslem) {
            mYeniIslem = false
            textViewHesap.text = " "
            mIlkDeger = mSonuc
        }
        val hane = textViewHesap.text.length
        if (hane < 15) {
            when (id) {

                R.id.button0 -> {
                    if (textViewHesap.getText().toString().equals(" ")) {
                        textViewHesap.text = textViewHesap.text.trim()
                    }
                    if (textViewHesap.text.length <= 1) {
                        if (!textViewHesap.text.toString().contains("0")) {
                            textViewHesap.setText(textViewHesap.text.toString() + "0")
                        }
                    } else {
                        textViewHesap.setText(textViewHesap.text.toString() + "0")
                    }
                }
                R.id.button1 -> textViewHesap.setText(textViewHesap.getText().toString() + "1")
                R.id.button2 -> textViewHesap.setText(textViewHesap.getText().toString() + "2")
                R.id.button3 -> textViewHesap.setText(textViewHesap.getText().toString() + "3")
                R.id.button4 -> textViewHesap.setText(textViewHesap.getText().toString() + "4")
                R.id.button5 -> textViewHesap.setText(textViewHesap.getText().toString() + "5")
                R.id.button6 -> textViewHesap.setText(textViewHesap.getText().toString() + "6")
                R.id.button7 -> textViewHesap.setText(textViewHesap.getText().toString() + "7")
                R.id.button8 -> textViewHesap.setText(textViewHesap.getText().toString() + "8")
                R.id.button9 -> textViewHesap.setText(textViewHesap.getText().toString() + "9")
                R.id.buttonNokta -> {
                    val value: String = textViewHesap.text.toString()
                    if (!value.equals("") && !value.equals(" ")) {
                        if (value.contains(".")) {
                            // bir nokta var
                        } else {
                            textViewHesap.setText(value + ".")
                        }
                    } else {
                        textViewHesap.setText(value + "0.")
                    }
                }
            }
        } else {
            Toast.makeText(this, getString(R.string.you_have_entered_the_maximum_number), Toast.LENGTH_SHORT).show()
        }
    }

    private fun buttonOperatorClicked(id: Int) {
        if (mYeniIslem) {
            mYeniIslem = false
            textViewHesap.text = " "
            mIlkDeger = mSonuc
        }

        when (id) {

            R.id.buttonTopla -> {
                mIslem = "Toplama"
                updateFirstValueAndClearHesap()
            }
            R.id.buttonCikarma -> {
                mIslem = "Cikarma"
                updateFirstValueAndClearHesap()
            }
            R.id.buttonCarpma -> {
                mIslem = "Carpma"
                updateFirstValueAndClearHesap()
            }
            R.id.buttonBolme -> {
                mIslem = "Bolme"
                updateFirstValueAndClearHesap()
            }

            R.id.buttonYuzde -> {
                mIslem = "Yuzde"
                updateFirstValueAndClearHesap()
            }
            R.id.buttonSifirla -> {
                clear()
            }
            R.id.buttonSil -> if (textViewHesap.length() > 0) {
                if (textViewHesap.length() == 1) {
                    textViewHesap.text = " "
                    return
                }
                textViewHesap.text = textViewHesap.text.substring(0, textViewHesap.text.length - 1)
            }
            R.id.buttonEsittir -> {
                calculate()
            }
        }
    }

    private fun calculate() {
        if (!textViewHesap.getText().toString().isEmpty() && mIlkDeger != null && !textViewHesap.getText().toString().equals(" ")) {
            if (mIslem.equals("Toplama", true)) {
                mSonuc = mIlkDeger!! + textViewHesap.getText().toString().toDouble()

            } else if (mIslem.equals("Cıkarma", true)) {
                mSonuc = mIlkDeger!! - textViewHesap.getText().toString().toDouble()

            } else if (mIslem.equals("Bolme", true)) {

                if (textViewHesap.getText().toString().toDouble() == 0.0) {
                    Toast.makeText(this,getString(R.string.the_number_is_not_divisible_by_zero), Toast.LENGTH_SHORT).show()
                    textViewHesap.setText(" ")
                    mIlkDeger = null
                    return

                } else {
                    mSonuc = mIlkDeger!! / textViewHesap.getText().toString().toDouble()
                }
            } else if (mIslem.equals("Carpma", true)) {
                mSonuc = mIlkDeger!! * textViewHesap.getText().toString().toDouble()

            } else if (mIslem.equals("Yuzde", true)) {
                mSonuc = (mIlkDeger!! / 100) * textViewHesap.getText().toString().toDouble()
            }

            textViewHesap.setText(mSonuc.toString())
            mYeniIslem = true
        } else {
            Toast.makeText(this, getResources().getString(R.string.enter_value), Toast.LENGTH_SHORT).show()
        }
    }

    private fun clear() {
        mIlkDeger = null

        mSonuc = null
        textViewHesap.text = " "
    }
}













