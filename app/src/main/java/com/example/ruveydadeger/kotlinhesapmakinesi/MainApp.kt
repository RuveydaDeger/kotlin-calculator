package com.example.ruveydadeger.kotlinhesapmakinesi

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Action_Man.ttf")  // default font
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
        Realm.init(this)
        var c = RealmConfiguration.Builder()
        c.name("Calculator DB")
        c.deleteRealmIfMigrationNeeded()
        Realm.setDefaultConfiguration(c.build())

    }
}