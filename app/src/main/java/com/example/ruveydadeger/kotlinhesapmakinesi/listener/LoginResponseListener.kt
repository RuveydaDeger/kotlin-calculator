package com.example.ruveydadeger.kotlinhesapmakinesi.listener


interface LoginResponseListener {
    fun onLoginSuccesful()
    fun onLoginFailure(error: String)
}
