package com.example.ruveydadeger.kotlinhesapmakinesi.`object`

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ListItem() : RealmObject() {
    @PrimaryKey open var value: String = ""
    var selected:Boolean = false

    constructor(value: String) : this() {
        this.value = value
    }
}